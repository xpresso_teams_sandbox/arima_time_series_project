"""
This is the implementation of data preparation for sklearn
"""

import sys
import numpy as np
import pandas as pd
from statsmodels.tsa.arima_model import ARIMA

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logging = XprLogger("train")


class Train(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="train")
        """ Initialize all the required constansts and data her """

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        dataset = pd.read_csv("/data/transformed_dataset/indexed_dataset.csv")
        # Parse strings to datetime type
        dataset['Month'] = pd.to_datetime(dataset['Month'],
                                          infer_datetime_format=True)  # convert from string to datetime
        indexedDataset = dataset.set_index(['Month'])
        print(indexedDataset.head(5))
        indexedDataset_logScale = np.log(indexedDataset)

        # order = (2,1,2)
        order_1 = sys.argv[2]
        order_2 = sys.argv[3]
        order_3 = sys.argv[4]
        order = (int(order_1), int(order_2), int(order_3))
        model = ARIMA(indexedDataset_logScale, order=order)
        results_ARIMA = model.fit(disp=-1)

        predictions_ARIMA_diff = pd.Series(results_ARIMA.fittedvalues,
                                           copy=True)
        print("predictions_ARIMA_diff:\n", predictions_ARIMA_diff.head(), flush=True)

        # Convert to cumulative sum
        predictions_ARIMA_diff_cumsum = predictions_ARIMA_diff.cumsum()
        print("predictions_ARIMA_diff_cumsum:\n", predictions_ARIMA_diff_cumsum, flush=True)

        predictions_ARIMA_log = pd.Series(
            indexedDataset_logScale['#Passengers'].iloc[0],
            index=indexedDataset_logScale.index)
        predictions_ARIMA_log = predictions_ARIMA_log.add(
            predictions_ARIMA_diff_cumsum, fill_value=0)
        print("predictions_ARIMA_log:\n", predictions_ARIMA_log.head(), flush=True)

        # Inverse of log is exp.
        predictions_ARIMA = np.exp(predictions_ARIMA_log)
        print("predictions_ARIMA:\n", predictions_ARIMA.head(), flush=True)
        ARIMA_freme = predictions_ARIMA.to_frame()
        print(ARIMA_freme.index)
        prediction_values = str(ARIMA_freme[0].iloc[-1]).split()
        print(prediction_values)
        self.send_metrics(dataset["Month"].iloc[-1], prediction_values[0].strip())
        self.completed()

    def send_metrics(self, prediction_date, prediction_value):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "Prediction completed"},
            "metric": {str(prediction_date): str(prediction_value) }
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        super().completed(push_exp=False)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = Train()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
